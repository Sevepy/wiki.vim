# Introduction

This [Vim](http://www.vim.org/)-plugin is a fork of [Wiki.Vim] with just the features relevant for my [website](https://tessarinseve.pythonanywhere.com/nws/index.html) workflow.


## Installation

Add the following line to the local `.vimrc.local.bundles` file:

```vim
Plug 'https://gitlab.com/sevepy/wiki.vim'
```


## Usage

Update the `vimrc.local` or the global `.vimrc` with the following options:
```vim
" wiki customization
let g:wiki_root = expand("%:p:h").'/Documents'
let g:wiki_filetypes=["wiki"]
"" control link style wiki ->[[]]
let g:wiki_link_target_type='wiki'
let g:wiki_map_link_create = 'ChangeExtension'

let g:wiki_export = {
    \ 'args' : '',          
    \ 'from_format' : 'markdown',
    \ 'ext' : 'pdf',
    \ 'view' : v:false,
    \ 'output': expand("%:p:h") ."/Documents",
    \}

function ChangeExtension(text) abort
    return substitute(a:text, ' ', '_', 'g') . '.wiki.html'
endfunction
```

# Features

It retains the following features compared to the full wiki.vim:

  - Global mappings for accessing a specified wiki
  - Local mappings for
    - Navigation (follow links, go back, etc)
    - Renaming pages (will also update links in other pages)
    - Creating a table of contents
    - Toggling links
    - Viewing wiki link graphs
  - Completion of wiki links and link anchors
  - Text objects
    - `iu au` Link url
    - `it at` Link text

- Support for journal entries
  - Navigating the journal back and forth with `<M-p>` and `<M-n>` (unresolved)

- Utility functionality
  - Toggling lists (marking as done/undone or add/remove TODO)
  - Text objects
    - `il al` List items

- Third-party support
  - [CtrlP](https://github.com/ctrlpvim/ctrlp.vim): `CtrlPWiki` command
  - [unite](https://github.com/Shougo/unite.vim)
  - [denite](https://github.com/Shougo/denite.nvim)



